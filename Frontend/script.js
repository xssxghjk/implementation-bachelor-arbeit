
window.onload = function() {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "http://localhost:8080", true);
    xhttp.onreadystatechange = function() {//Call a function when the state changes.
        if(xhttp.readyState === 4 && xhttp.status === 200) {
            createView(this.responseText);
        } else {
            console.log("Request Error...");
        }
    };
    xhttp.send();
};

function createView(schedulejson) {
    var scheduleobj = JSON.parse(schedulejson);
    console.log(scheduleobj);
    var routeTimes = scheduleobj.routeTimes;
    var timeslots = scheduleobj.timeslots;
    var timeslotsCount = scheduleobj.countTimeslots;
    var routeDurations = scheduleobj.routeDurations;
    var success = scheduleobj.success;
    var message = scheduleobj.message;
    var requestString = scheduleobj.requestString;
    createScheduleTable(routeTimes, timeslots, timeslotsCount, routeDurations);
    createFeedback(success, message, requestString)
}

function createFeedback(success, message, reuqestString) {
    var container = document.getElementsByClassName("container")[0];
    var successSpan = document.createElement("p");
    var messageSpan = document.createElement("p");
    var requestSpan = document.createElement("p");
    var borderbox = document.createElement("div");
    borderbox.classList.add("border");
    successSpan.innerText = "Optimization was successful: " + success;
    messageSpan.innerText = "Message: " + message;
    requestSpan.innerText = "Request Text: " + reuqestString;
    borderbox.appendChild(successSpan);
    borderbox.appendChild(messageSpan);
    borderbox.appendChild(requestSpan);
    container.appendChild(borderbox);
}

function createScheduleTable(rTimes, tSlots, tSlotsCount, rDurations){
    let truckCount = rTimes.length;
    let chargingP = document.createElement("p");
    chargingP.innerText = "Charging";
    // create tablehead
    let thead = document.getElementById("tablehead");
    let theadrow = document.createElement("tr");
    let emptyth = document.createElement("th");
    theadrow.appendChild(emptyth);
    for(let i = 0; i < tSlotsCount; i++) {
        let thead = document.createElement("th");
        let tdiv = document.createElement("div");
        thead.innerText = i;
        theadrow.appendChild(thead);
    }
    thead.appendChild(theadrow);
    // create tablebody
    let tbody = document.getElementById("tablebody");
    for(let i = 0; i < rTimes.length; i++) {
        let trow = document.createElement("tr");
        let trucktd = document.createElement("td");
        let tdiv = document.createElement("div");
        tdiv.classList.add("square");
        tdiv.innerText = "Truck" + i;
        trucktd.classList.add("truck");
        trucktd.appendChild(tdiv);
        trow.appendChild(trucktd);
        for(let j = 0; j < tSlotsCount; j++) {
            let td = document.createElement("td");
            let div = document.createElement("div");
            div.id = "ts" + i.toString() + j.toString();
            div.classList.add("square");
            div.innerText = "Waiting";
            td.appendChild(div);
            trow.appendChild(td);
        }
        tbody.append(trow);
    }
    let routeindex = 0;
    for(let i = 0; i < tSlots.length; i++) {
        let currentTSlot = tSlots[i];
        // add timeslots
        for(let j = 0; j < currentTSlot.length; j++) {
            if(currentTSlot[j] === 1) {
                let div = document.getElementById("ts" + i.toString() + j.toString());
                div.classList.add("charging");
                div.innerText = "Charging";
            }
        }
        // add routes
        for(let j = 0; j < rTimes[i].length; j++) {
            for(let n = 0; n < rDurations[routeindex]; n++) {
                console.log("ts"+ i.toString() + (rTimes[i][j] + n));
                let div = document.getElementById("ts"+ i.toString() + (rTimes[i][j] + n));
                div.classList.add("route");
                div.innerText = "Route "+routeindex;
            }
            routeindex++;
        }
    }
}