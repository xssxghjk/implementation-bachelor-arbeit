let invalid = document.createElement("div");
invalid.classList.add("invalid-feedback");
invalid.innerText = "Enter an Integer!";
let err;
let truckbox;
let routebox;
window.onload = function() {
    truckbox = document.getElementsByClassName("truckbox")[0].cloneNode(true);
    routebox = document.getElementsByClassName("routebox")[0].cloneNode(true);
}


function onSubmit() {
    err = false;
    let trucks = document.getElementsByClassName("truckbox");
    let solSchedule = {
        trucks: [],
        countChargingStations: 0,
        chargePerTime: 0,
    };
    for(let ii = 0; ii < trucks.length; ii++) {
        let solTruck = {
            initialCharge: 0,
            routes: [],
        };
        let truck = trucks[ii];
        let routes = truck.getElementsByClassName("routebox");
        let tInputs = truck.getElementsByTagName("input");
        let trInput = tInputs[0];
        if(checkInput(trInput)){
            solTruck.initialCharge = parseInt(trInput.value);
        }
        for(let jj = 0; jj < routes.length; jj++) {
            let solRoute = {
                plannedDeparture: 0,
                duration: 0,
                requiredCharge: 0,
            };
            let route = routes[jj];
            let rInputs = route.getElementsByTagName("input");
            let plannedDeparture = rInputs[0];
            let duration = rInputs[1];
            let requiredCharge = rInputs[2];
            if (checkInput(plannedDeparture)) {
                solRoute.plannedDeparture = parseInt(plannedDeparture.value);
            }
            if (checkInput(duration)) {
                solRoute.duration = parseInt(duration.value);
            }
            if (checkInput(requiredCharge)) {
               solRoute.requiredCharge = parseInt(requiredCharge.value);
            }
            solTruck.routes.push(solRoute);
        }
        solSchedule.trucks.push(solTruck);
    }
    let countChargingStations = document.getElementById("countchargingstations");
    let chargePerTime = document.getElementById("chargepertime");
    if(checkInput(countChargingStations)){
        solSchedule.countChargingStations = parseInt(countChargingStations.value);
    }
    if(checkInput(chargePerTime)){
        solSchedule.chargePerTime = parseInt(chargePerTime.value);
    }
    if(!err) {
        sendRequest(solSchedule)
    }
}

function sendRequest(data) {
    console.log(data);
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "http://localhost:8080", true);
    xhttp.setRequestHeader('Content-Type', 'text/plain');
    xhttp.onreadystatechange = function() {//Call a function when the state changes.
        if(xhttp.readyState === 4 && xhttp.status === 200) {
            window.location.href = "scheduleview.html";
        } else {
            console.log("Request Error...")
        }
    };
    xhttp.send(JSON.stringify(data));
}

function addTruck() {
    let truckcontainer = document.getElementById("truckcontainer");
    truckcontainer.appendChild(truckbox.cloneNode(true));
}

function addRoute(event) {
    let routecontainer = event.srcElement.parentElement.parentElement.getElementsByClassName("routecontainer")[0];
    routecontainer.appendChild(routebox.cloneNode(true));
}

function checkInput(input) {
    if(input.value === "") {
        if (input.parentElement.getElementsByClassName("is-invalid").length === 0) {
            let newinvalid = invalid.cloneNode(true);
            input.parentElement.appendChild(newinvalid);
            input.classList.add("is-invalid")
        }
        err = true;
        return false;
    }
    if(input.parentElement.getElementsByClassName("is-invalid").length !== 0){
        input.parentElement.removeChild(input.parentElement.getElementsByClassName("invalid-feedback")[0]);
        input.classList.remove("is-invalid");
    }
    return true;
}

function onTextSubmit() {
    let textInput = document.getElementById("requesttext");
    let request = JSON.parse(textInput.value);
    sendRequest(request)
}

            /*
           let rInput = rInputs[0];
           rInput.parentElement.appendChild(invalid);
           rInput.classList.add("is-invalid")
           */