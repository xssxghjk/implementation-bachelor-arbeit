import numpy as np
from scipy.optimize import minimize
import copy
import math
import json
from http.server import HTTPServer, BaseHTTPRequestHandler
import time

# DataStructs:


class Route:
    indexCounter = 0
    def __init__(self, planneddeparture, duration, requiredcharge):
        self.plannedDeparture = planneddeparture
        self.duration = duration
        self.requiredCharge = requiredcharge
        self.index = self.indexCounter
        self.indexCounter += 1


class Schedule:
    def __init__(self, trucks, countchargestations, counttimeslots, durationtimeslot, chargepertime):
        self.trucks = trucks
        self.countChargeStations = countchargestations
        self.countTimeSlots = counttimeslots
        self.durationTimeSlot = durationtimeslot
        self.chargePerTime = chargepertime
        self.routes = []
        for truck in trucks:
            for route in truck.routes:
                self.routes.append(route)


class Truck:
    indexCounter = 0
    def __init__(self, initialcharge, maxcharge, routes):
        self.initialCharge = initialcharge
        self.maxCharge = maxcharge
        self.routes = routes
        self.index = self.indexCounter
        self.indexCounter += 1

# ------------------------------------------------------------------------------------------------------


# Algorithm:


def objective(x, schedule):
    sol = roundList(x)
    val = 0
    routes = schedule.routes.copy()
    for index, el in enumerate(sol):
        if index < len(routes):
            val += abs(el - routes[index].plannedDeparture)
    return val

'''
Enough charge for each route:
initialCharge + sum(timeslots) * chargeSpeed - sum(route.cost)
type: ineq
'''
def constraint1(x, schedule, truck, route, indexroute, indextruck):
    sol = roundList(x)
    chargeSpeed = schedule.chargePerTime
    initialCharge = truck.initialCharge
    routeIndex = truck.routes.index(route)
    # All routes including and up to current route
    routes = truck.routes[0:routeIndex + 1]
    # Sum of all route costs up to current route
    sumRouteCosts = 0
    # Number of timeslots up to current route
    NumberTimeslots = sol[indexroute]
    # Sum of all timeslot values of x up to current route
    sumTimeslots = 0
    for route in routes:
        sumRouteCosts += route.requiredCharge
    for index, value in enumerate(sol):
        if indextruck <= index <= indextruck + NumberTimeslots:
            sumTimeslots += round(value)
    solution = initialCharge + sumTimeslots * chargeSpeed - sumRouteCosts
    return solution


'''
for every timeslot no more than n charging trucks
type: ineq
'''
def constraint2(x, schedule, timeslot):
    sol = roundList(x)
    truckStartinIndex = len(schedule.routes)
    timeslotLength = schedule.countTimeSlots
    chargeTimeslot = 0
    for index, el in enumerate(schedule.trucks):
        chargeTimeslot += round(x[timeslot + truckStartinIndex 
        + timeslotLength * index])
    return schedule.countChargeStations - chargeTimeslot



'''
for every route truck cant  be charged while gone
type: eq
'''
def constraint3(x, schedule, route, indexRoute, indexTruck):
    sol = roundList(x)
    routeStartingTime = sol[indexRoute]
    routeTimeslot = indexTruck + routeStartingTime
    duration = route.duration
    sum = 0
    for i in range(duration):
        if routeStartingTime + i < schedule.countTimeSlots:
            sum += round(sol[int(routeTimeslot + i)])
    return -sum

'''
for every route: actualDeparture cannot be before plannedDeparture
type: ineq
'''
def constraint4(x, route, routeindex):
    sol = roundList(x)
    ret = sol[routeindex] - route.plannedDeparture
    return ret

'''
FOR EVERY ROUTE: CAN ONLY BE PROCESSED IF NO OTHER ROUTE IS BEING PROCESSED
'''
def constraint5(x, truck, route, indexRoute):
    ret = 0
    currRouteIndex = truck.routes.index(route)
    maxIndex = len(truck.routes) - 1
    if currRouteIndex != maxIndex:
        sol = roundList(x)
        routeStartingTime = sol[indexRoute]
        nextRouteStartingTime = sol[indexRoute + 1]
        duration = route.duration
        ret = nextRouteStartingTime - (routeStartingTime + duration)
    return ret




'''
for every timeslot, only charge if it does not reach truck capacity
type: ineq
WIP, does not work

def constraint5(x, schedule, truck, route, indexroute, indextruck, timeslot):
    sol = roundList(x)
    chargeSpeed = schedule.chargePerTime
    initialCharge = truck.initialCharge
    routeIndex = truck.routes.index(route)
    # All routes including and up to current route
    routes = truck.routes[0:routeIndex + 1]
    # Sum of all route costs up to current route
    sumRouteCosts = 0
    # Number of timeslots up to current route
    NumberTimeslots = sol[indexroute] #CHANGE THIS PROBABLY...
    # Sum of all timeslot values of x up to current route
    sumTimeslots = 0

    for route in routes:
        sumRouteCosts += route.requiredCharge

    for index, value in enumerate(sol):
        if indextruck <= index <= timeslot:
            sumTimeslots += round(value)
    solution = initialCharge + sumTimeslots * chargeSpeed - sumRouteCosts
    return truck.maxCharge - solution
'''
# ------------------------------------------------------------------------------------------------------


# Getters for lambda functions

def getObjectiveFunction(schedule):
    return lambda x, : objective(x, schedule)

def getConstraints(schedule):
    constraints = []
    constraints.extend(getConstraint1(schedule))
    constraints.extend(getConstraint2(schedule))
    constraints.extend(getConstraint3(schedule))
    constraints.extend(getConstraint4(schedule))
    constraints.extend(getConstraint5(schedule))
    return constraints


def getConstraint1(schedule):
    constraints = []
    for truck in schedule.trucks:
        for route in truck.routes:
            indexRoute = schedule.routes.index(route)
            indexTruck = len(schedule.routes) + (schedule.countTimeSlots * schedule.trucks.index(truck))
            constraint = lambda x, schedule=schedule, truck=truck, route=route, indexRoute=indexRoute, indexTruck=indexTruck:\
                constraint1(x, schedule, truck, route, indexRoute, indexTruck)
            constraints.append({'type': 'ineq', 'fun': constraint})
    return constraints

def getConstraint2(schedule):
    constraints = []
    for i in range(schedule.countTimeSlots):
        constraints.append({'type': 'ineq', 'fun': lambda x, schedule=schedule, i=i: constraint2(x, schedule, i)})
    return constraints


def getConstraint3(schedule):
    constraints = []
    for truck in schedule.trucks:
        for route in truck.routes:
            indexRoute = schedule.routes.index(route)
            indexTruck = len(schedule.routes) + (schedule.countTimeSlots * schedule.trucks.index(truck))
            constraints.append({'type': 'ineq', 'fun': lambda x, schedule=schedule, route=route, indexRoute=indexRoute, indexTruck=indexTruck:
                               constraint3(x, schedule, route, indexRoute, indexTruck)})
    return constraints


def getConstraint4(schedule):
    constraints = []
    for index, route in enumerate(schedule.routes):
        constraints.append({'type': 'ineq', 'fun': lambda x, route=route, index=index: constraint4(x, route, index)})
    return constraints

# (x, truck, route, indexRoute)
def getConstraint5(schedule):
    constraints = []
    for truck in schedule.trucks:
        for route in truck.routes:
            indexRoute = schedule.routes.index(route)
            constraints.append({'type': 'ineq', 'fun': lambda x, schedule=schedule, truck=truck, route=route, indexRoute=indexRoute:
            constraint5(x, truck, route, indexRoute)})
    return constraints

'''
# WIP, does not work yet!
def getConstraint5(schedule):
    constraints = []
    for truck in schedule.trucks:
        for route in truck.routes:
            indexRoute = schedule.routes.index(route)
            indexTruck = len(schedule.routes) + (schedule.countTimeSlots * schedule.trucks.index(truck))
            constraint = lambda x, schedule=schedule, truck=truck, route=route, indexRoute=indexRoute, indexTruck=indexTruck: \
                constraint1(x, schedule, truck, route, indexRoute, indexTruck)
            constraints.append(constraint)
    return constraints
'''



def getBounds(schedule):
    boundRoute = [0, schedule.countTimeSlots-1]
    boundTimeslot = [0, 1]
    countRoutes = len(schedule.routes)
    countTimeslots = len(schedule.trucks) * schedule.countTimeSlots
    bounds = []
    for i in range(countRoutes):
        bounds.append(boundRoute)
    for j in range(countTimeslots):
        bounds.append(boundTimeslot)
    return bounds

def roundList(x):
    ret = []
    for el in x:
        ret.append(int(round(el)))
    return ret

def createSimpleSolution(schedule):
    sol = []
    routeTimes = []
    timeSlots = []
    trucks = schedule.trucks
    numberOfTimeslots = 0
    for truckIndex, truck in enumerate(trucks):
        prevRoutes = []
        timeSlots.append([])
        routes = truck.routes
        for routeIndex, route in enumerate(routes):
            prevRoutes.append(route)
            routesCharge = 0
            chargeSlots = 0
            for prevRoute in prevRoutes:
                routesCharge += prevRoute.requiredCharge
            for slot in timeSlots[truckIndex]:
                chargeSlots += slot
            diffCharge = truck.initialCharge - routesCharge + (chargeSlots * schedule.chargePerTime)
            if diffCharge >= 0:
                if route.plannedDeparture > len(timeSlots[truckIndex]):
                    diff = route.plannedDeparture - len(timeSlots[truckIndex])
                    for i in range(diff):
                        timeSlots[truckIndex].append(0)
                routeTimes.append(len(timeSlots[truckIndex]))
                for i in range(route.duration):
                    timeSlots[truckIndex].append(0)
            else:
                neededSlots = int(abs(math.ceil(abs(diffCharge / schedule.chargePerTime))))
                for slot in range(neededSlots):
                    isOccupied = True
                    while isOccupied:
                        chargedTrucks = 0
                        for slotlist in timeSlots:
                            if len(slotlist) > len(timeSlots[truckIndex]):
                                chargedTrucks += 1
                        if chargedTrucks > schedule.countChargeStations:
                            timeSlots[truckIndex].append(0)
                        else:
                            timeSlots[truckIndex].append(1)
                            isOccupied = False
                if route.plannedDeparture > len(timeSlots[truckIndex]):
                    diff = route.plannedDeparture - len(timeSlots[truckIndex])
                    for i in range(diff):
                        timeSlots[truckIndex].append(0)
                routeTimes.append(len(timeSlots[truckIndex]))
                for i in range(route.duration):
                    timeSlots[truckIndex].append(0)
    if len(timeSlots[truckIndex]) > numberOfTimeslots:
            numberOfTimeslots = len(timeSlots[truckIndex])
    sol.extend(routeTimes)
    for index,route in enumerate(schedule.routes):
        if (sol[index] + route.duration) > numberOfTimeslots:
            numberOfTimeslots = sol[index] + route.duration
    for sl in timeSlots:
        for j in range(numberOfTimeslots - len(sl)):
            sl.append(0)
        sol.extend(sl)
    schedule.countTimeSlots = numberOfTimeslots
    return sol, schedule





'''

Variablen: 	    - Für jede Route ein abfahrtszeitpunkt
                - Für jeden Truck für jeden Zeitpunkt, ob dieser geladen wird (0 nicht geladen,
                 1 geladen)
                Also:   x0-xn-1:    Abfahrtszeitpunkte 
                        xn-xm:      Timeslots geladen
                        n = Anzahl Routen
                        m = Anzahl Timeslots * Anzahl Trucks


Restriktion:	-Immer genug ladung für jede Route (gilt für jede route)
                -Eine Ladesäule kann nur 1 LKW gleichzeitig geladen werden 
                (für jeden timeslot: geladene lkws <= ladesäulenanzahl)
                -Ein LKW kann nicht doppelt geladen werden (mit dieser aufstellung 
                allgemein nicht möglich)
                -Ein LKW kann unterwegs nicht geladen werden (für alle Trucks gilt:
                 alle timeslots von Routen Abfahrtszeit bis dauerzeit muss 0 sein)
                -Maximale Ladung darf nicht überschritten werden (für jeden truck
                 für jeden timeslot, problem: läd nicht auf das maximum), alternativ: bei der 
                 ladungsberechnung cap einbauen (GUTE IDEE :D)
                -Abfahrtzeitpunkt darf nicht vor geplanter abfahrt sein
                
                


'''

# main:


def createSchedule(input):
    print(input);
    trucklist = []
    inputTrucks = input["trucks"]
    for truck in inputTrucks:
        inputRoutes = truck["routes"]
        routelist = []
        for route in inputRoutes:
            routelist.append(Route(route["plannedDeparture"], route["duration"], route["requiredCharge"]))
        trucklist.append(Truck(truck["initialCharge"], 0, routelist))
    schedule = Schedule(trucklist, input["countChargingStations"], 0, 0, input["chargePerTime"])
    return schedule
'''
t1Routes = [Route(0, 2, 30.0), Route(4, 3, 50.0)]
truck1 = Truck(30.0, 150.0, t1Routes)

t2Routes = [Route(1, 1, 15.0), Route(5, 2, 55.0)]
truck2 = Truck(20.0, 100.0, t2Routes)

t3Routes = [Route(0, 3, 60.0)]
truck3 = Truck(65.0, 150.0, t3Routes)

t4Routes = [Route(3, 2, 55.0)]
truck4 = Truck(30.0, 80.0, t4Routes)

t5Routes = [Route(0, 2, 30.0), Route(4, 3, 50.0)]
truck5 = Truck(30.0, 150.0, t5Routes)

schedule = Schedule([truck1, truck2, truck3, truck4, truck5], 2
                    , 7, 120, 25.0)
'''


def isSolutionValid(sol, schedule):
    cons = getConstraints(schedule)
    for el in cons:
        if el.get('fun')(sol) < 0:
            return False
    return True


def calculateOptimalSolution(Schedule, reqDataStr, eps=1, ftol=1, isEval=False):
    simpleSolution, updatedSchedule = createSimpleSolution(Schedule)
    print(updatedSchedule.countTimeSlots)
    print("Simple Solution created.")
    print("Is simple solution valid: " + str(isSolutionValid(simpleSolution, updatedSchedule)))
    print(simpleSolution)
    finalSolution = simpleSolution
    bnds = getBounds(updatedSchedule)
    print("Bounds created.")
    cons = getConstraints(updatedSchedule)
    print("Constrains created..")
    obj = getObjectiveFunction(updatedSchedule)
    print("Objective function created.")
    print("ACC:" + str(ftol))
    print("alpha: " + str(eps))
    startTime = time.time()
    solution = minimize(obj,simpleSolution,method='SLSQP', bounds=bnds,constraints=cons, options={'eps': eps, 'ftol': ftol})
    endTime = time.time()
    deltaT = endTime - startTime
    x = solution.x
    print("Optimizing complete.")
    print("Status: " + str(solution.success))
    print("Message: " + solution.message)
    print("Initial Cost: " + str(round(objective(simpleSolution, updatedSchedule))))
    if solution.success:
        print("Optimized Cost: " + str(int(round(objective(x, Schedule)))))
        finalSolution = roundList(x)
    routeTimes = []
    timeslots = []
    routeDurations = []
    index = 0
    print(finalSolution)
    print("Is final solution valid: " + str(isSolutionValid(finalSolution, updatedSchedule)))
    for i,el in enumerate(updatedSchedule.trucks):
        routeTimes.append(finalSolution[index: index+len(el.routes)])
        index += len(el.routes)
        timeslots.append(finalSolution[len(updatedSchedule.routes)+(i*updatedSchedule.countTimeSlots):len(updatedSchedule.routes)+((i+1)*updatedSchedule.countTimeSlots)])
    for el in updatedSchedule.routes:
        routeDurations.append(el.duration)

    res = {
        "routeTimes": routeTimes,
        "routeDurations": routeDurations,
        "timeslots": timeslots,
        "countTimeslots": updatedSchedule.countTimeSlots,
        "success": str(solution.success),
        "message": solution.message,
        "requestString": reqDataStr,
    }
    print("Response data type created.")
    evalData = {}
    if isEval:
        #create data needed for evaluation
        evalData = {
            "succ": str(solution.success),
            "z": updatedSchedule.countTimeSlots,
            "par": len(simpleSolution),
            "bed": len(cons),
            "d": deltaT,
            "f(x0)": str(int(round(objective(simpleSolution, updatedSchedule)))),
            "f(xn)": str(int(round(objective(x, Schedule)))),
            "diff": str(int(round(objective(simpleSolution, updatedSchedule))) - int(round(objective(x, Schedule))))
        }
        return res, evalData

    return res




# This function was used for the evaluation chapter
def evaluate(reqDataStr, alpha, ACC):
    reqData = json.loads(reqDataStr)
    Schedule = createSchedule(reqData)
    rAnz = len(Schedule.routes)
    rDauer = 0
    rKosten = 0
    if len(Schedule.routes) > 0:
        for route in Schedule.routes:
            rDauer += route.duration
            rKosten += route.requiredCharge
        rDauer = rDauer / len(Schedule.routes)
        rKosten = rKosten / len(Schedule.routes)
    tAnz = len(Schedule.trucks)
    tLad = 0
    if len(Schedule.trucks) > 0:
        for truck in Schedule.trucks:
            tLad += truck.initialCharge
        tLad = tLad / len(Schedule.trucks)
    lGes = Schedule.chargePerTime
    lAnz = Schedule.countChargeStations
    print("Eingabeparameter:")
    print("  r:         " + str(rAnz))
    print("  rDauer:    " + str(rDauer))
    print("  rKosten:   " + str(rKosten))
    print("  t:         " + str(tAnz))
    print("  tLad:      " + str(tLad))
    print("  lGes:      " + str(lGes))
    print("  lAnz:      " + str(lAnz))
    print("  Alpha:     " + str(alpha))
    print("  ACC:       " + str(ACC))
    responseData, evalData = calculateOptimalSolution(Schedule, reqDataStr, alpha, ACC, True)
    print(evalData)
    print(responseData)


'''
reqDataStr = '{"trucks":[{"initialCharge":30,"routes":[{"plannedDeparture":0,"duration":2,"requiredCharge":20},{"plannedDeparture":3,"duration":2,"requiredCharge":30}]},{"initialCharge":0,"routes":[{"plannedDeparture":2,"duration":1,"requiredCharge":20},{"plannedDeparture":4,"duration":2,"requiredCharge":25}]},{"initialCharge":0,"routes":[{"plannedDeparture":3,"duration":3,"requiredCharge":30}]},{"initialCharge":20,"routes":[{"plannedDeparture":1,"duration":2,"requiredCharge":30},{"plannedDeparture":5,"duration":1,"requiredCharge":20}]},{"initialCharge":0,"routes":[{"plannedDeparture":3,"duration":2,"requiredCharge":20}]}, {"initialCharge":0,"routes":[{"plannedDeparture":4,"duration":2,"requiredCharge":30}]}, {"initialCharge":25,"routes":[{"plannedDeparture":0,"duration":1,"requiredCharge":20},{"plannedDeparture":3,"duration":3,"requiredCharge":40}]}, {"initialCharge":0,"routes":[{"plannedDeparture":2,"duration":2,"requiredCharge":50}]}, {"initialCharge":25,"routes":[{"plannedDeparture":1,"duration":1,"requiredCharge":40},{"plannedDeparture":4,"duration":1,"requiredCharge":40}]}],"countChargingStations":6,"chargePerTime":25}'
alpha = 1
ACC = 1

evaluate(reqDataStr, alpha, ACC)
'''
'''

Schedule = createTestData()
Sol = createSolution(Schedule)
# print(isSolutionValid(Sol, Schedule))
Sol, Schedule = createSimpleSolution(Schedule)

# PROBLEM: TIMESLOT COUNT NOT HIGH ENOUGH, FIX SOMEHOW...
ObjectiveLambda = getObjectiveFunction(Schedule)
bnds = getBounds(Schedule)
cons = getConstraints(Schedule)
obj = getObjectiveFunction(Schedule)
print(len(Sol))
solution = minimize(obj,Sol,method='SLSQP', bounds=bnds,constraints=cons, options={'eps': 1, 'ftol': 1})
x = solution.x
print(isSolutionValid(x, Schedule))
# show final objective
print(solution.success)
print(solution.message)
print('Final SSE Objective: ')
# print solution
print('Initial Sol ' + str(round(objective(Sol, Schedule))))
print('Solution ' + str(round(objective(x, Schedule))))

x = roundList(x)
routeTimes = []
timeslots = []
routeDurations = []
index = 0
for i,el in enumerate(Schedule.trucks):
    routeTimes.append(x[index: index+len(el.routes)])
    index += len(el.routes)
    timeslots.append(x[len(Schedule.routes)+(i*Schedule.countTimeSlots):len(Schedule.routes)+((i+1)*Schedule.countTimeSlots)])
for el in Schedule.routes:
    routeDurations.append(el.duration)

print(timeslots)
res = {
    "routeTimes": routeTimes,
    "routeDurations": routeDurations,
    "timeslots": timeslots,
    "countTimeslots": Schedule.countTimeSlots
}
retStr = json.dumps(res)
print(retStr)

for el in x:
    print(round(el))
'''



from http.server import HTTPServer, BaseHTTPRequestHandler


responseData = {
    "routeTimes": [],
    "routeDurations": [],
    "timeslots": [],
    "countTimeslots": 0,
    "success": "NoData",
    "message": "No data was posted yet...",
    "requestString": "",
}

class Serv(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()

    def do_OPTIONS(self):
        self.send_response(200, "ok")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type")
        self.end_headers()

    def do_GET(self):
        global responseData
        response = bytes(json.dumps(responseData), "utf-8") #create response
        self.send_response(200) #create header
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type")
        self.send_header("Content-Length", str(len(response)))
        self.end_headers()
        self.wfile.write(response) #send response

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        reqDataStr = post_data.decode("utf-8")
        print(reqDataStr)
        reqData = json.loads(reqDataStr)
        schedule = createSchedule(reqData)
        global responseData
        responseData = calculateOptimalSolution(schedule, reqDataStr)

        self.send_response(200) #create header
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type")
        #self.send_header("Content-Length", str(len(response)))
        self.end_headers()

        #self.wfile.write(response) #send response


print("Starting server: 'localhost:8080")
httpd = HTTPServer(('localhost', 8080), Serv)
httpd.serve_forever()




'''
t1Routes = [Route(0, 2, 30.0), Route(4, 3, 50.0)]
truck1 = Truck(30.0, 150.0, t1Routes)

t2Routes = [Route(1, 1, 15.0), Route(5, 2, 55.0)]
truck2 = Truck(20.0, 100.0, t2Routes)

t3Routes = [Route(0, 3, 60.0)]
truck3 = Truck(65.0, 150.0, t3Routes)

t4Routes = [Route(3, 2, 55.0)]
truck4 = Truck(30.0, 80.0, t4Routes)

t5Routes = [Route(0, 2, 30.0), Route(4, 3, 50.0)]
truck5 = Truck(30.0, 150.0, t5Routes)

schedule = Schedule([truck1, truck2, truck3, truck4, truck5], 2
                    , 7, 120, 25.0)


sol, schedule = createSimpleSolution(schedule)
cons = getConstraint5(schedule)
for el in cons:
    el.get('fun')(sol)

'''
'''
reqDataStr = '{"trucks":[{"initialCharge":30,"routes":[{"plannedDeparture":0,"duration":2,"requiredCharge":20},{"plannedDeparture":3,"duration":2,"requiredCharge":30}]},{"initialCharge":0,"routes":[{"plannedDeparture":2,"duration":1,"requiredCharge":20},{"plannedDeparture":4,"duration":2,"requiredCharge":25}]},{"initialCharge":0,"routes":[{"plannedDeparture":3,"duration":3,"requiredCharge":30}]},{"initialCharge":20,"routes":[{"plannedDeparture":1,"duration":2,"requiredCharge":30},{"plannedDeparture":5,"duration":1,"requiredCharge":20}]},{"initialCharge":0,"routes":[{"plannedDeparture":3,"duration":2,"requiredCharge":20}]}],"countChargingStations":2,"chargePerTime":25}'
print(reqDataStr)
reqData = json.loads(reqDataStr)
schedule = createSchedule(reqData)
sol, schedule = createSimpleSolution(schedule)
print(len(schedule.routes))
print(schedule.countTimeSlots)
print(sol)
print(isSolutionValid(sol, schedule))
'''
