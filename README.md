Benötigte Programme:
	-Python 3.X, getestet mit 3.7.2
	-JavaScript-Fähiger Browser, getestet mit Chrome Version 72

Starten der Applikation:
	1. Im "./Server" Ordner "server.py" ausführen (python server.py)
	2. Im "./Frontend" Ordner über den Browser "Index.html" öffnen

Nutzen der Applikation:
-Eingabe der Daten:
	Zunächst müssen die Daten für das Ladesäulenmanagements eingegeben werden. Dies kann durch die Eingabemaske eingetragen und über "Submit" bestätigt werden.
	Alternativ kann dies auch direkt über JSON eingegeben und über "Submit Text" bestätigt werden. Beispieldaten hierzu findet man in "./Fontend/optExample.txt". Diese sind auch die Daten, die für die Evaluation der Arbeit genutzt wurden.
-Berechnung der Lösung:
	Sobald die Daten gesendet wurden, beginnt die Rechnung auf dem Server. Informationen hierzu werden auf dem Server ausgegeben.
-Ausgabe der Lösung:
	Sobald die Berechnung abgeschlossen ist, wird diese auf dem Frontend ausgegeben. Falls ein neues Schedule eingegeben werden soll, kann der Knopf link oben genutzt werden.

Bekannte Fehler:
	- Bei bestimmten Eingabewerten wird ein falscher Startwert x0 berechnet. Hierbei werden im ersten Timeslot mehr E-Trucks geladen, als Ladesäulen zur verfügung stehen. Dies führt dazu, dass keine Optimierung stattfinden kann, da der Startwert fehlerhaft ist.
	